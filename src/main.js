import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';

import GameMenu from './views/GameMenu';
import GamePlay from './views/GamePlay';
import GameOver from './views/GameOver';

Vue.use(VueRouter);

Vue.config.productionTip = false;

const routes = [
  {
    path: '/',
    component: GameMenu,
    name: 'startpage'
  },
  {
    path: '/gameplay',
    component: GamePlay
  },
  {
    path: '/gameover',
    component: GameOver,
    name: 'gameover'
  }
];

const router = new VueRouter({
    routes
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
